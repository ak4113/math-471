+++++++++
Lecture 2
+++++++++
 
**Version control and GIT**

What is version control?
========================

* The simplest form of version control is to keep multiple copies of files and  directories. 
* The problem with this approach is that it is hard for **you** to remember if ``file1.f90``, ``filea.f90`` or ``file1a.f90`` is the latest version. 
* Moreover, it is **impossible** for your **collaborators** to know! 
* Rather than coming up with our own creative file / directory naming scheme we can have Version Control Software do it (and much more) for us.


Why version control?
====================

* Leads to reproducible results. For example you can get back *exactly* the version of a code that you used for some paper or report. 
* Safety. With a VCS you will most likely store your files on multiple computers. Hard disk crashes **do happen!** 
* If you work on multiple computers version control is a way of synchronizing your files. 
* Makes collaboration easier. Different persons can work on the same project at the same time. 
* Not limited to software. 


Client-server VCS
=================

* In a  client-server system one computer, the server, keeps all the files in the repository and all other computers, the clients, check out and in the code. 
* The original version control program CVS and the more modern Subversion (or SVN) are two VCS that use this model. 
* A drawback of this model is that it requires connectivity and that there is a risk that all code, or at least the history of the code, to be lost if the server crashes. 


Distributed VCS
===============

* Modern VCS like GIT and Mercurial are distributed systems where all local repositories contain all the files and the full history of the repo.  
* The distributed approach is more resilient to hardware crashes and you can actually still be working on your project and committing changes even if you happen to be Internet-less!

GIT
===

* Originally developed by Linus Torvalds for the development for the Linux kernel
* It is easy to learn the basic work-cycle to get started
* but it also have many advanced features and is highly flexible
* It is modern and popular, thus there are tons of podcasts and tutorials
* In this course you will only use the basic features but feel free to learn more on your own

Bitbucket
=========

* It is possible to use git for local version control on a single computer but it is more common to also host a public or private repository on a remote server. 
* There are many free git-hosting options like ``git-hub``, ``bitbucket``, ``code.google``, etc. 
* Bitbucket is convenient for this course as it allows for multiple private repositories  
* The ``Bitbucket 101`` tutorial is a good place to start if you are new to version control


Newton's method
===============

* Suppose we want to solve an equation  :math:`\ \ f(x) = 0`
* That is, we want to find the intersection with the x-axis.

.. figure:: newton1.png
   :height: 450px


Approximation by a straight line
================================

* Everyone knows how solve :math:`\ \ ax+b=0` so let's put the problem :math:`\ \ f(x) = 0` on that form.
* We will use the straight line that has a slope :math:`\ \ f'(x_0)` and that passes through :math:`\ \ f(x_0)`:
* :math:`y = f'(x_0)(x-x_0) + f(x_0)`
* Here :math:`x_0` is a starting guess and we solve :math:`y=0` for a new value :math:`x_1`
* :math:`0 = f'(x_0)(x_1-x_0) + f(x_0)`
* :math:`x_1 = x_0 - f(x_0)/f'(x_0)`

Approximation by a straight line
================================

.. figure:: newton2.png
   :height: 450px

* This can be repeated in an iterative fashion.
 
