+++++++++
Lecture 3
+++++++++
 
**How to report the homework**

Rule 1!
=======

  1. You are NOT!!! done with the homework when the code
     compiles. That is just the first 15% of the homework! 
  2. Just because the code compiles, that does not mean it is bug
     free.
  3. Always make sure you have time to do computations that convince
     you and (more importantly) me and David (our TA) that the code
     works.
  4. Make sure you have time to do computations that are needed for
     the report.
  5. Make sure you have time to write the report. We will grade on
     correctness of the code, results and the exposition of the
     material. 



How to debug
============

  0. Turn on the most agressive and strict warning flags on your
     compiler.
  1. In scientific computing we typically know how the method / code
     should behave in the sense of convergence / numerical
     approximation error. Make sure you check it and that you
     include those results in the report.
  2. For example if your code approximates a derivative with a forward
     difference :math:`f'(x) = (f(x+h)-f(x))/h` then you would expect
     that the error :math:`e(h) = | f'(x) - (f(x+h)-f(x))/h | \approx
     C\times h`.
  3. How to display :math:`e(h) = C \times h^q`?
  4. Recall :math:`log(e(h)) = log(C) + q \times log(h)`.



Reporting errors in a figure
============================

.. figure:: errors1.jpg
   :height: 450px

This figure is bad! Too much whitespace, uses ``plot(h,h,h,h.^2)``.

A bit better
============

.. figure:: errors2.jpg
   :height: 450px

This uses ``loglog(h,h,h,h.^2)``
 
Better still
============

.. figure:: errors3.jpg
   :height: 450px

Added a legend and made lines wider.



Acceptable for a report
=======================

.. figure:: errors4.jpg
   :height: 450px

Legend, wider lines, ``loglog``, grid and labels on the axis. 
 
