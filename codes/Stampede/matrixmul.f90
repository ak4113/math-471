program matrixmul
  use omp_lib
  implicit none 
  integer, parameter :: nmax = 800
  integer :: i,j,k,l,myid
  real(kind = 8), dimension(nmax,nmax) :: A,B,C
  real(kind = 8) :: d,time1,time2,time1a,time2a
  A = 1.d0
  B = 2.d0
  
  do l = 1,8
     !$ call omp_set_num_threads(l)
     call cpu_time(time1)
     time1a = omp_get_wtime()
     !$OMP PARALLEL DO PRIVATE(i,j,k,d)
     do j = 1,nmax
        do i = 1,nmax
           d = 0.d0  
           do k = 1,nmax
              d =  d + A(i,k)*B(k,j)
           end do
           C(i,j) = d
        end do
     end do
     call cpu_time(time2)
     time2a = omp_get_wtime()
     write(*,*) "With ", l, " threads this takes: ",time2-time1 , " of cpu_time but only ",time2a-time1a, " wall clock time."  
  end do
  
end program matrixmul
