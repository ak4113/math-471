!
! Simple program that illustrates: 
! do loops, if-then-else, logical and numeric comparisons
! 

program loops
  implicit none
  integer, parameter :: n = 3
  real(kind = 8), parameter :: pi = 3.141592653589793238462643383279d0
  integer :: i 
  real(kind = 8) :: a
  real :: c = 0.
  logical :: correct 
  do i = 1,n
     if ( i < 2) then
        c = c + pi
        a = abs(dble(i)*pi-c)/(dble(i)*pi)
        correct = .false.
     elseif (i == 2) then
        a = (i/n)*pi
        correct = .false.
     else
        a = (i/n)*pi
        correct = .true.
     endif
     print *, i, a, correct
  end do
end program loops
