program HOWDY
  use mpi
  implicit none
  integer :: ierr, nprocs, myid
  integer, parameter :: nmax = 10000
  integer :: size,tag1,tag2
  real(kind =8) :: m_out(nmax),m_in(nmax)
  integer :: status1(MPI_STATUS_SIZE),status2(MPI_STATUS_SIZE),request1,request2
  
  call mpi_init(ierr)
  call mpi_comm_size(MPI_COMM_WORLD, nprocs, ierr)
  call mpi_comm_rank(MPI_COMM_WORLD, myid, ierr)

  do size = 1,nmax,1
     if(myid == 0) then
        m_out = dble(size)
        call MPI_ISend(m_out,size,MPI_DOUBLE_PRECISION,1,&
             0,MPI_COMM_WORLD,request1,ierr)
        call MPI_IRecv(m_in,size,MPI_DOUBLE_PRECISION,1,&
             MPI_ANY_TAG,MPI_COMM_WORLD,request2,ierr)
        !write(*,*) 'Proc 0 not done, size is: ', size, maxval(m_in(1:size))-dble(size)
     end if
     if(myid == 1) then
        m_out = dble(size)
        call MPI_ISend(m_out,size,MPI_DOUBLE_PRECISION,0,&
             0,MPI_COMM_WORLD,request2,ierr)
        call MPI_IRecv(m_in,size,MPI_DOUBLE_PRECISION,0,&
             MPI_ANY_TAG,MPI_COMM_WORLD,request1,ierr)
        !write(*,*) 'Proc 1 not done, size is: ', size, maxval(m_in(1:size))-dble(size)
     end if
     call mpi_wait(request1,status1,ierr)
     call mpi_wait(request2,status2,ierr)
     if(myid == 1) then
        write(*,*) 'Proc 1 done, size is: ', size, maxval(m_in(1:size))-dble(size)
     end if
     if(myid == 0) then
        write(*,*) 'Proc 0 done, size is: ', size, maxval(m_in(1:size))-dble(size)
     end if

  end do
  call mpi_finalize(ierr)
end program HOWDY
