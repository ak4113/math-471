program HOWDY
  use mpi
  implicit none
  integer :: ierr, nprocs, myid
  integer, parameter :: nmax = 10000
  integer :: size,tag1,tag2
  real(kind =8) :: m_out(nmax),m_in(nmax)
  integer :: status(MPI_STATUS_SIZE)
  
  call mpi_init(ierr)
  call mpi_comm_size(MPI_COMM_WORLD, nprocs, ierr)
  call mpi_comm_rank(MPI_COMM_WORLD, myid, ierr)

  do size = 1,nmax,1
     if(myid == 0) then
        call MPI_Send(m_out,size,MPI_DOUBLE_PRECISION,1,&
             0,MPI_COMM_WORLD,ierr)
        call MPI_Recv(m_in,size,MPI_DOUBLE_PRECISION,1,&
             MPI_ANY_TAG,MPI_COMM_WORLD,status,ierr)
        write(*,*) 'Proc 0 done, size is: ', size
     end if
     if(myid == 1) then
        call MPI_Recv(m_in,size,MPI_DOUBLE_PRECISION,0,&
             MPI_ANY_TAG,MPI_COMM_WORLD,status,ierr)
        call MPI_Send(m_out,size,MPI_DOUBLE_PRECISION,0,&
             0,MPI_COMM_WORLD,ierr)
        write(*,*) 'Proc 1 done, size is: ', size
     end if
  end do
  call mpi_finalize(ierr)
end program HOWDY
