clear

x_dir = dir('x*.txt')
u_dir = dir('sol*.txt');
n_proc  = length(x_dir);
n_steps = length(u_dir)/n_proc;

X = [];
for j = 1:n_proc
    x = load(x_dir(j).name);
    X = [X x];
end

ii = 1;
for  it = 1:n_steps
    U = [];
    for j = 1:n_proc
        u = load(u_dir(ii).name); 
        U =[U u];
        ii = ii + 1;
    end
    plot(X,U)
    axis([-1 1 -1 1])
    drawnow
end