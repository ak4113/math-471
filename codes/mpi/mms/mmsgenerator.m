clear all

nd = 3;

syms x y t

cx = [1 1 0 0 0 0 0 0 0];
cy = [1 1 0 0 0 0 0 0 0];
ct = [1 1 0 0 0 0 0 0 0];

px = cx(1)*(1+0*x);
py = cy(1)*(1+0*y);
pt = ct(1)*(1+0*t);

for i = 1:nd
  px = px+cx(i+1)*x^i;
  py = py+cy(i+1)*y^i;
  pt = pt+ct(i+1)*t^i;
end

px = cos(pi*x);
py = cos(pi*y);
pt = cos(pi*t);

for it=0:nd
  for ix=0:nd
    for iy=0:nd
      Z(ix+1,iy+1,it+1) = diff(px,x,ix)*diff(py,y,iy)*diff(pt,t,it);
    end
  end
end

fortran(Z, 'file', 'mms.f')