program weq
  use mpi
  implicit none
  integer :: ierr, nprocs, myid
  integer :: status(MPI_STATUS_SIZE)
  
  integer, parameter :: nx = 50
  real(kind = 8) :: t_final = 3.d0
  integer, parameter :: m = 5
  integer :: nr,nrl,i,j,nt,it
  
  integer :: px,ix_off ! what is my x-proc index and what is my offset
  integer :: p_left,p_right,px_max
  integer :: p_down,p_up,py_max
  integer :: nxl,nyl !local size
  integer :: remx,remy
  real(kind = 8) :: hr,hx,t,dt,y_tmp,tp,max_err_loc,max_err
  real(kind = 8), dimension(:),  allocatable :: r,x,u,up,um,uxx,force,uex
  integer :: int_sum
  CHARACTER(7) :: char_step
  CHARACTER(7) :: char_id

  real(kind = 8) :: umms(0:m,0:m,0:m)
  
  call mpi_init(ierr)
  call mpi_comm_size(MPI_COMM_WORLD, nprocs, ierr)
  call mpi_comm_rank(MPI_COMM_WORLD, myid, ierr)
  
  nr = nx
  hr = 2.d0/dble(nr-1)
  
  ! Label the processes from 1 to px_max
  px = myid + 1
  px_max = nprocs
  
  ! Split up the grid in the "x-direction"
  nxl = nx/px_max
  remx = nx-nxl*px_max
  if (px .le. remx) then
     nxl = nxl + 1
     ix_off = (px-1)*nxl
  else
     ix_off = (remx)*(nxl+1) + (px-(remx+1))*nxl
  end if
  call MPI_BARRIER(MPI_COMM_WORLD,ierr)
  call MPI_Reduce(nxl,int_sum,1,&
       MPI_INTEGER,MPI_SUM,&
       0,MPI_COMM_WORLD,ierr)
  if(myid == 0) then
     if (nx .ne. int_sum) then
        write(*,*) 'Something is wrong with the number of points in x-direction: ',&
             nx,int_sum
     end if
  end if
  nrl = nxl
  ! Who are my neighbours? 
  ! NOTE THAT WE ARE MAPPING TO THE 1D INDEXING HERE!!!!
  p_left  = px-1 - 1
  p_right = px+1 - 1
  if (px .eq. px_max) p_right = MPI_PROC_NULL
  if (px .eq. 1) p_left = MPI_PROC_NULL  

  ! Allocate memory for the various arrays
  allocate(r(nrl),u(0:nrl+1),up(0:nrl+1),um(0:nrl+1))
  allocate(x(0:nrl+1),uxx(1:nrl),force(1:nrl),uex(1:nrl))
  do i = 1,nrl
     r(i) = -1.d0 + dble(i-1+ix_off)*hr
  end do
  do i = 1,nrl
     x(i) = r(i)
  end do

  ! send to the left recieve from the right
  call MPI_Sendrecv(x(1),1,MPI_DOUBLE_PRECISION,p_left,123,&
       x(nrl+1),1,MPI_DOUBLE_PRECISION,p_right,123,MPI_COMM_WORLD,status,ierr)
  ! send to the right recieve from the left
  call MPI_Sendrecv(x(nrl),1,MPI_DOUBLE_PRECISION,p_right,125,&
       x(0),1,MPI_DOUBLE_PRECISION,p_left,125,MPI_COMM_WORLD,status,ierr)
  call MPI_BARRIER(MPI_COMM_WORLD,ierr)

  t = 0.d0
  hx = hr
  ! Compute the timestep
  dt = 0.9d0*hx
  nt = floor(t_final/dt)+1
  dt = t_final/dble(nt)
  
  WRITE(char_id,"(I7.7)") myid
  call printdble1d(x(1:nrl),nrl,"x"//trim(char_id)//".txt")

  ! Set up initial data (include the ghost points)
  do i = 0,nxl+1
     call mms(umms,x(i),0.d0,t-dt,m)
     um(i) = umms(0,0,0)
     call mms(umms,x(i),0.d0,t,m)
     u(i) = umms(0,0,0)
  end do
  ! Do some time stepping
  do it = 1,nt
     ! Communicate between processors
     ! send to the left recieve from the right
     call MPI_Sendrecv(u(1),1,MPI_DOUBLE_PRECISION,p_left,123,&
          u(nrl+1),1,MPI_DOUBLE_PRECISION,p_right,123,MPI_COMM_WORLD,status,ierr)
     ! send to the right recieve from the left
     call MPI_Sendrecv(u(nrl),1,MPI_DOUBLE_PRECISION,p_right,125,&
          u(0),1,MPI_DOUBLE_PRECISION,p_left,125,MPI_COMM_WORLD,status,ierr)
     call MPI_BARRIER(MPI_COMM_WORLD,ierr)
     ! Then Physical BC
     if (px .eq. px_max) then
        call mms(umms,x(nxl),0.d0,t,m)
        u(nxl) = umms(0,0,0)
     end if
     if (px .eq. 1) then
        call mms(umms,x(1),0.d0,t,m)
        u(1) = umms(0,0,0)
     end if
     ! Compute the right hand side
     call compute_uxx(uxx,u,nxl,hx,t,x)
     call compute_forcing(force,nxl,t,x)
     ! Compute the new solution 
     up(1:nxl) = 2.d0*u(1:nxl) - um(1:nxl) + dt**2*(uxx+force) 
     ! Increment time and swap the time levels
     t = t+dt
     um = u
     u  = up
     call MPI_BARRIER(MPI_COMM_WORLD,ierr)
     ! Update physical BC
     if (px .eq. px_max) then
        call mms(umms,x(nxl),0.d0,t,m)
        u(nxl) = umms(0,0,0)
     end if
     if (px .eq. 1) then
        call mms(umms,x(1),0.d0,t,m)
        u(1) = umms(0,0,0)
     end if
     WRITE(char_id,"(I7.7)") myid
     WRITE(char_step,"(I7.7)") it
     call printdble1d(u(1:nrl),nrl,&
          "sol"//trim(char_step)//"_"//trim(char_id)//".txt")
     ! Compute exact solution
     do i = 1,nxl
        call mms(umms,x(i),0.d0,t,m)
        uex(i) = umms(0,0,0)
     end do
     ! Compute the max error
     max_err_loc = maxval(abs(u(1:nxl)-uex))
     call MPI_Reduce(max_err_loc,max_err,1,&
          MPI_DOUBLE_PRECISION,MPI_MAX,&
          0,MPI_COMM_WORLD,ierr)
     if(myid.eq.0) write(*,*) "Time: ", t, max_err 
  end do
  call mpi_finalize(ierr)
end program WEQ

subroutine compute_forcing(force,nx,t,x)
  implicit none
  integer :: nx
  real(kind = 8) :: force(1:nx),t,x(0:nx+1)
  real(kind = 8) :: umms(0:2,0:2,0:2)
  integer :: i
  
  do i = 1,nx
     call mms(umms,x(i),0.d0,t,2)
     force(i) = umms(2,0,0)-umms(0,0,2)
  end do
  
end subroutine compute_forcing

subroutine compute_utt(utt,u,nx,hx,t,x)
  implicit none
  integer :: nx
  real(kind = 8) :: u(0:nx+1),x(0:nx+1),hx,t
  real(kind = 8) :: utt(1:nx),uxx(1:nx)
  call compute_uxx(uxx,u,nx,hx,t,x)
  utt = uxx
  
end subroutine compute_utt

subroutine compute_uxx(uxx,u,nx,hx,t,x)
  implicit none
  integer :: nx
  real(kind = 8) :: u(0:nx+1),x(0:nx+1),hx,uxx(1:nx),t
  integer :: i
  real(kind = 8) :: h2i

  h2i = 1.d0/hx/hx
  do i = 1,nx
     uxx(i) = h2i*(u(i+1)-2.d0*u(i)+u(i-1))
  end do
  
end subroutine compute_uxx


subroutine printdble1d(u,nx,str)
  implicit none
  integer, intent(in) :: nx
  real(kind = 8), intent(in) :: u(nx)
  character(len=*), intent(in) :: str
  integer :: i
  open(2,file=trim(str),status='unknown')
  do i=1,nx,1
     write(2,fmt='(E24.16)',advance='no') u(i)
  end do
  close(2)
end subroutine printdble1d
   
