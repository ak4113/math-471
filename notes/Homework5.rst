.. -*- rst -*- -*- restructuredtext -*-

.. _HWK5:
   
=================================
Homework 5, due 08.00, 15/10-2015
=================================

Angry Birds: Numerical Solution of ODE
--------------------------------------

This homework is concerned with a problem of immense societal
importance; the simulation of angry birds. 

Let the location of bird number :math:`k` be :math:`B_k(t) =
(x_k(t),y_k(t))`, obviously the bird leader is :math:`B_1(t)`.

The bird leader does not really care too much about the other birds
but is primarily interested in eating `birdy nam nam`__ being handed
out by the crazy bird feeder at :math:`C(t) = (x_c(t),y_c(t))`. At any
given point in time :math:`B_1(t)` is thus trying to reduce its
distance to :math:`C(t)` by 

.. math::
   :nowrap:

   B'_1(t) = \gamma_1 (C(t)-B_1(t)),

where :math:`\gamma_1` is a positive constant that depends on how good
the food tastes. 

Simulate this equation for some randomly chosen initial locations for
the bird leader using the classic fourth order Runge-Kutta
method. Explain in words what the meaning of the equation is
describing, for example what does it mean when :math:`x_c > x_1`? Try
with some different examples of :math:`C(t)` including :math:`C(t) =
(0,0)` and :math:`C(t) = (\sin(a t),\cos(a t))`. 

The rest of the birds are trying to stay close to the leader and are
therefore governed by 

.. math::
   :nowrap:

   B'_k(t) = \gamma_2 (B_1(t)-B_k(t)) ,\ \ k = 2,\ldots,n_{\rm Birds},

where :math:`\gamma_2` depends on how charismatic the bird leader
is.

There is another mechanism that is important to the birds and that
is to be as close as possible to the middle of the flock or
birds. This will be safer if the flock is attacked by a predator that
will try to single out a lonely bird to catch. Let the center of gravity
be denoted :math:`\bar{B}` then to each right hand side you can add

.. math::
   :nowrap:

   \kappa (\bar{B}(t)-B_k(t)),\ \ k = 2,\ldots,n_{\rm Birds}.

Of course if the birds get too close to each other they cannot
maneuver properly so there is also a strong repelling force. If bird
number :math:`k` is repelled by its :math:`L` closest neighbors we
can add to the right hand sides 

.. math::
   :nowrap:

   \sum_{l=1}^L \rho \frac{(B_k(t)-B_l(t))}{(B_k(t)-B_l(t))^2+\delta},\ \ k = 2,\ldots,n_{\rm Birds}.

For this assignment the main objective is to experiment with the
mathematical model. The possibilities are endless: 
   
   1. Experiment with different parameters and different number of birds. Can you explain how the different parameters impact the behaviour of the flock?

   2. How does the diameter (what is a reasonable definition?) change over time and for different scenarios?

   3. How does the dynamic change if you have shared leadership?

   4. Add a smelly bird that want to be at the center but is strongly
      repellant to the other birds. 

   5. How does the numerical method impact the results? Try Forward
      Euler. Try different time steps.

   6. Can you add a predator that eats the birds but gets slower as it
      gets fatter and fatter?

   7. Can you model something else? A social network, a soccer game,
      spread of diseases between individuals based on their travel
      patterens, and so on...   

   8. Your own extensions...
      
Write up a nice report with your findings. Add some movies of Angry
birds to your repository.
      
__ https://youtu.be/4vuW6tQ0218
