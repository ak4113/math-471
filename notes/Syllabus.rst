========
Syllabus
========

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
Introduction to Scientific Computing - Math 471 - Fall 2015
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

**Time and Place:**

12:30-13:15, TR, Science & Math Learning Center B59. 

**Instructor:**

Daniel Appelo, 277-3310, appelo `kanelbulle`__ math.unm.edu 

__ http://en.wikipedia.org/wiki/Cinnamon_roll

**Optional texts:**

1.  *V. Eijkhout (with E. Chow and R. van de Geijn)*, Introduction to High Performance Scientific Computing (Lulu, 2010),
2.  *G. Em Karniadakis, and R. M. Kirby II*, Parallel Scientific Computing in C++ and MPI (Cambridge University Press, 2003).

Note that the first text is available `here`__. 

__ https://bitbucket.org/VictorEijkhout/hpc-book-and-course/src

**Office hours:**

SMLC 310 TR 13.30-15.30

**Prerequisites:**

Math 314 or 321, and Math 316 and programming skills.

**Description and goals:**

This is an introductory course in scientific computing which will cover a number of topics. The philosophy of the course is *wide and shallow*, meaning that I will attempt to expose you to many different concepts, ideas and technologies rather than focus on deeper study of a limited number of subjects. 

The goals of this class are: 
   
   * To acquire practical and theoretical knowledge of software-tools that allow you to write, document and optimize computer programs. 
   * To understand how the machine architecture impacts of the performance and the design of computer programs. 
   * To have hands on experience using scripting to automate your computations and the analysis of computational results.    
   * To understand the basic concepts of parallel computing and to write parallel programs for shared, distributed and hybrid memory models. 

Hopefully, by being exposed to many different topics you will be able to better tackle future problems that you encounter in your studies or career.

**Grading:**

Your grade for this course is based on homework and computing projects, in-class work/attendance, and a final project/exam, in the following proportion: Homework/computing projects 80%, final project / final exam 20%. 

After the weighted percentage grade has been calculated as detailed above, letter grades will be assigned according to the following scheme: A, 90 or above, B, 80 or above, C, 70 or above, D, 60 or above, F below 60. However, the instructor reserves the right to "curve" grades to offset unforeseen circumstances. The curving of grades will never decrease a student's letter grade below that given by the above formula.

**Homework / Computer Projects:** 

The homework will consist of theoretical and computational assignments that you will hand in. You are *strongly* encouraged to work in pairs for the homework but it is expected that both of you can explain the material. For the write-ups of the assignments we will use a version control system. 

**Dishonesty policy:**

Each student is expected to maintain the highest standards of honesty and integrity in academic and professional matters. The University reserves the right to take disciplinary action, including dismissal, against any student who is found responsible for academic dishonesty. Any student who has been judged to have engaged in academic dishonesty in course work may receive a reduced or failing grade for the work in question and/or for the course.
Academic dishonesty includes, but is not limited to, dishonesty on quizzes, tests or assignments; claiming credit for work not done or done by others; and hindering the academic work of other students.

**American disabilities act:**

In accordance with University Policy 2310 and the American Disabilities Act (ADA), academic accommodations may be made for any student who notifies the instructor of the need for an accommodation. It is imperative that you take the initiative to bring such needs to the instructor's attention, as the instructor is not legally permitted to inquire. Students who may require assistance in emergency evacuations should contact the instructor as to the most appropriate procedures to follow. Contact Accessibility Services at 505-661-4692 for additional information

**Disclaimer:**

I reserve the right to make reasonable and necessary changes to the policies outlined in this syllabus. Whenever possible, the class will be notified well in advance of such changes. An up-to-date copy of the syllabus can always be found on the course website. It is your responsibility to know and understand the policies discussed therein. If in doubt, ask questions.

