.. _lab2:

===============================
Laboratory Session 2 
===============================

 - **If you get stuck, ask for help from me or David.**
 - Feel free to talk to each other and help each other as needed.
 - Use your imagination to extend the assignments and experiment.


During this lab session you can either work on Homework 3 that is due on Thursday or you can start working on Homework 4. A good starting point is to familiarize yourself with the different programs in the Gaussian quadrature example.

    
    
