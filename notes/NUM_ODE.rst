.. -*- rst -*- -*- restructuredtext -*-

.. _NUM_ODE:

=========================
Numerical methods for ODE
=========================

As we have seen time ad time again a useful tool from calculus for designing numerical methods is Taylor's formula

.. math::
   :nowrap:

   f(z) = f(a) + \frac{x-a}{1!} f'(a) + \frac{(x-1)^2}{2!} f^{(2)}(a) + \ldots + \frac{(x-1)^{n-1}}{(n-1)!}  + R_n,

where the reminder is

.. math::
   :nowrap:

   \frac{(x-1)^{n}}{n!} f^{(n)}(\xi), \ \ \xi \in [a,x].

Alternatively, by instead expanding $f(z)$ around $z+h$, we find that

.. math::
   :nowrap:

   f(z+h) = f(z) + h f'(z) + \frac{h^2}{2!} f''(z) + \mathcal{O} (h^3).


Now if we are looking to solve an ordinary differential equation

.. math::
   :nowrap:

   \dot{u}(t) = f(u(t),t),\ \  u(0) = u_0,
 
we can use the above formulas to approximate the time derivative on the left

.. math::
   :nowrap:

    \frac{u(t+\Delta t)-u(t)}{\Delta t}  \approx \dot{u}(t) = f(u(t),t). 


Rearranging the equation allows us to compute :math:`u_n \equiv u({t_n}) = u(n \Delta t)` according to

.. math::
   :nowrap:

   \begin{eqnarray*}
   u_0 &=& u_0, \\
   u_{n+1} &=& u_n + \Delta t \, f(u_n,t_n), \ \ n= 0,1,\ldots
   \end{eqnarray*}

The above method is called **forward Euler**.

Accuracy and stability 
-----------------------
It turns out that if we compute the solution up to some fixed time the
error decreases with order of accuracy of the above method is one, or
in other words the error is proportional to :math:`\Delta t`.

This might be surprising as the error in the approximation of one time step is actually proportional to :math:`(\Delta t)^2`. Roughly speaking the explanation goes like this, if we compute to some time :math:`T` with a time step :math:`\Delta` we have to take :math:`N_t = \frac{T}{\Delta t}` steps, if we commit an error of order :math:`(\Delta t)^2` in each of those steps the final error can be as large as :math:`T \Delta t` and hence of order of accuracy one.

In any case, any numerical method for solving ODEs has an error expansion which, assuming there is a maximum allowable error, sets an upper limit on :math:`\Delta t`. This is the accuracy limit. To illustrate the stability limit we consider the following exercise.

Exercise
--------
There is also an upper limit on :math:`\Delta t` that is dictated by
stability considerations. Consider the special case when
:math:`f(u(t),t) = \lambda u(t), \ \ \lambda < 0` and :math:`u_0 = 1`


  1. Find the solution to

     .. math::
	:nowrap:

	\dot{u}(t) = \lambda u(t), \ \ u(0) = 1.

  2. What is the steady state? Is the solution of the continuous problem stable?

  3. Use Euler's method to compute the approximate solution at the first few time steps. What is the stability limit on :math:`\Delta t`?    

  4. Repeat the analysis (applied to :math:`f(u(t),t) = \lambda u(t)`) for **backward Euler** 

     .. math::
	:nowrap:
     
	\begin{eqnarray*}
	u_0 &=& u_0, \\
	u_{n+1} &=& u_n + \Delta t \, f(u_{n+1},t_{n+1}), \ \ n= 0,1,\ldots
	\end{eqnarray*}
	   
     When can this method be useful? What is the additional complication compared to forward Euler when applied to an ODE with a more complicated right hand side?


