.. -*- rst -*- -*- restructuredtext -*-

.. _homework1:

================================
Homework 1, due 08.00, 27/8-2015
================================

Make sure you also invite **dbizzoze** to your repository. 


Version control and Git
--------------------------------

1.  Sign up for a free personal account at `bitbucket.org`_.
2.  Create a *private* Git repository with a unique name (don't use Math471.)
3.  Set up Git on your machine if you haven't already. There are good instructions on the official `git-scm.com`_. It should also be possible to use ``linux.unm.edu`` where Git should be installed.
4.  Follow the instructions for command line, "I'm starting from scratch".

    * Set up your local directory.
    * ``mkdir /path/to/your/project``
    * ``cd /path/to/your/project``
    * ``git init``
    * ``git remote add origin https://username@bitbucket.org/username/your_repo.git``

    * Create your first file, commit, and push 
    * ``echo "Jane Doe" >> contributors.txt``
    * ``git add contributors.txt``
    * ``git commit -m 'Initial commit with contributors'``
    * If you are working on ``linux.unm.edu`` then execute ``export GIT_SSL_NO_VERIFY=true`` before the next step.
    * ``git push -u origin master``

5.  Now we will clone the repository you just created. Go to the root of your home directory (i.e. type ``cd``) and create a directory where you keep all the repositories, e.g. ``mkdir repos`` and change into that directory (``cd repos``.)
 
6.  Next point your browser to your repository on `bitbucket.org`_. To the left there is a actions menu, click on *clone* and copy the text (something like ``git clone https://username@bitbucket.org/username/your_repo.git``) to your terminal window and hit enter. You should now have a directory with your repository that you can go into. 

7.  In the directory ``your_repo`` create a file called ``README.rst``. The extension ``.rst`` indicates that this file is written in `reStructuredText`_. To hand in the homework in this class you will send me a ``SHA-1 checksum`` that identifies the version of your repository that you want me to grade (more about this soon). Upon grading I will first read the readme file where you will put instructions where I can find the report and code for each homework. For example it could contain something like: 

.. code-block:: none
   :linenos:

   +++++++++++++++++++++++++++++++++
   README file for Jane Doe Math 471
   +++++++++++++++++++++++++++++++++

   1. Homework 1: 
   
      - The report is in Homework1/Report and is called homework1.pdf.
   
      - The code for this homework is located in Homework1/Code 
        and is documented in the appendix of the report.

8.  Once you are done editing your readme file you will first add it to your local git repository, commit locally and then push your changes to your remote repository on bitbucket.
    
    - ``git add README.rst``
    - ``git commit -m "Added a README"``
    - ``git push``

9.  You can inspect your activity by ``git log`` it should look something like this: 

.. code-block:: none
   :linenos:
   
      commit 84e7a3e57bf0e10dbb9a998d55b5976e55514ad3
   Author: Daniel Appelo <appelo@polaris.unm.edu>
   Date:   Tue Aug 5 09:47:27 2014 -0600

       Added a README

   commit 78db2b57948e1778e62cd9118e78ee83f6f0af82
   Author: Daniel Appelo <appelo@polaris.unm.edu>
   Date:   Mon Aug 4 17:39:28 2014 -0600

       Initial commit with contributors

10.  Play around with git and add some directory structure and files to your repository, perhaps homework folders for the first few assignments. 

11.  Add a version of "Hello World!" in your favourite language in ``Homework1/Code`` along with a report describing how to compile and run the program. 

12.  To submit your homework for grading first use the *invite* button on `bitbucket.org`_ to invite the user ``appelo`` with read privileges. Then, when you are ready to submit (before the due time / date), send me an email with the SH1 checksum from the appropriate commit (use ``git log`` to get the checksum.)  


.. _bitbucket.org: https://bitbucket.org
.. _git-scm.com: http://git-scm.com
.. _reStructuredText: http://en.wikipedia.org/wiki/ReStructuredText

