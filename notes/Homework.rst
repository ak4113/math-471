
======================
 Homework Instructions
======================

The homework will consist of theoretical and computational assignments that you will hand in via version control. You are *strongly* encouraged to work in pairs for the homework but it is expected that both of you can explain the material. For the write-ups of the assignments we will use a version control system. 

Typically you will hand in computer code and a written report. The grade will depend on the quality of both.  