.. -*- rst -*- -*- restructuredtext -*-

.. _ODE_MODELING:

=====================
Mathematical Modeling
=====================

An important aspect of applied mathematics is to develop mathematical models (equations) that describe phenomena and processes in nature, this is called mathematical modeling. Mathematical modeling is the third pillar of science and is today equally important to theoretical analysis and experimentation.

To a large extent the success of mathematical modeling is due to the fields of numerical analysis and scientific computing whose aim it is to translate the equations used in the mathematical models into algorithms that are possible to implement on computers. These algorithms are then able to produce predictions about real world processes and phenomena. The fidelity of the predictions depends on the mathematical model as well as the power of the computer that is used to produce the predictions. It also depends, often more critically, on the ingenuity of the numerical methods used in the algorithms. For example, solving an :math:`n \times n` linear system of equations requires :math:`\sim n^3` floating point operations and single core on a modern computer can perform on the order of :math:`3\times 10^9` operations per second. Thus if we would like to solve a system with :math:`n=1,000,000` unknowns it would take :math:`10^{18} / 10^9 / 60 / 60 / 24 / 365 \approx 31` years. On the other hand, if we use the **incredibly ingenious** multigrid method which only requires about :math:`C\times n` operations to solve the same system it would instead take about :math:`C` thousands of a second!

An important consequence of this kind of reduction in the use of resources is that the time to solution has been reduced leading to much shorter design and inovation cycles in engineering and production.

The mathematical modeling process
---------------------------------

Typically the mathematical modeling process starts with a set of rules that are thought to be valid for the problem at hand. For example when modeling a fluid flow the conservation of mass is such a rule.

These rules can be even simpler and not even motivated by physics as in the case of economics. A simple rule that you use every day is the conservation of money. Suppose you go to the store to buy granola and that you have $10 to spend and that the granola costs $2.87 per lbs. How much granola can you buy? One way to approach this problem is to write it as an equation: :math:`x` lbs of granola costs :math:`2.87x` and I have $10. Then conservation of money states that :math:`2.87x = 10` or in other words you can buy 10/2.87 lbs of granola.

For more complicated problems the models are more complicated and it may not always be clear what rules one should choose to enforce. Then the modeling process could become iterative a loop

 1. Choose a model.
 2. Use numerical analysis to discretize the model and turn it into an algorithm.
 3. Simulate the problem and compare the solution to experimental data to verify the model. If the results needs to be improved go back to 1. and try to improve the model, and/or to 2. and improve the simulation.

In the homework :ref:`HWK5` you will experiment with some different models for the group dynamics of a flock of birds.






