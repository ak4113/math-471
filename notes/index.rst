.. Math471 documentation master file, created by
   sphinx-quickstart on Mon Aug  4 12:47:10 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


Math/CS 471: Introduction to Scientific Computing, Fall 2015.
=============================================================

This course is an introductory course in scientific computing and will cover a wide variety of topics, some of them are listed below. 

In designing the course I have been inspired by Randall J. LeVeque's course `AMath 483/583`__ so if you find that your are missing some information on these pages or if you want a slightly different description of some topic his `class notes`__ should be a good source of information. 

__ http://faculty.washington.edu/rjl/classes/am583s2014/notes/index.html
__ http://faculty.washington.edu/rjl/classes/am583s2014/notes/index.html

The course repository where these notes and other files from class can be found is  `here`__.

__ https://bitbucket.org/appelo/math-471

The textbook *Introduction to High-Performance Scientific Computing* by Victor Eijkhout is available `here`__. 

__ https://bitbucket.org/VictorEijkhout/hpc-book-and-course/src

**Virtual machine**

You can download the virtual machine from `here`__. You will need to install
VirtualBox to use it.  

__ http://www.math.unm.edu/~appelo/teaching/Math471F15/Linux471Clone.zip


**News**

   * Homework 1 has been updated with David's username **dbizzoze**
     make sure you invite both of us to your repository.

   * We are about to start with OpenMP. There are good tutorials `here`__ and `here`__.  

__ http://www.openmp.org/presentations/miguel/F95_OpenMPv1_v2.pdf
__ https://computing.llnl.gov/tutorials/openMP/


**General information:**

.. toctree::
   :maxdepth: 1
   
   Syllabus
   
**Homework:**

.. toctree::
   :maxdepth: 1

   Homework
   Homework1
   Homework2
   Homework3
   Homework4
   Homework5
   Homework5A
   Homework7

   
**Laboratory work:**

.. toctree::
   :maxdepth: 1

   Labsession1
   Labsession2
   
**Slides:**

   * `Slides for lecture 1`__.
   * `Slides for lecture 2`__.
   * `Slides for lecture 3`__.
   * `Slides for lecture 4`__.
   * `Slides for lecture 5`__.
   * `Slides for lecture 6`__.
   * `Slides for lecture 7`__.
   * `Slides for lecture 20`__.

__ http://www.math.unm.edu/~appelo/teaching/Math471F15/slides/Lecture1.html
__ http://www.math.unm.edu/~appelo/teaching/Math471F15/slides/Lecture2.html
__ http://www.math.unm.edu/~appelo/teaching/Math471F15/slides/Lecture3.html
__ http://www.math.unm.edu/~appelo/teaching/Math471F15/slides/Lecture4.html
__ http://www.math.unm.edu/~appelo/teaching/Math471F15/slides/Lecture5.html
__ http://www.math.unm.edu/~appelo/teaching/Math471F15/slides/Lecture6.html
__ http://www.math.unm.edu/~appelo/teaching/Math471F15/slides/Lecture7.html
__ http://www.math.unm.edu/~appelo/teaching/Math471F15/slides/Lecture20.html

**Technical topics:**

.. toctree::
   :maxdepth: 1
   
   LINUX
   VersionControl
   GIT
   perl
   FORTRAN_BASIC
   FiniteDifferences
   FORTRAN_FD_EXAMPLE
   ODE_MODELING
   NUM_ODE
   STAMPEDE

..   
..   
   Fortran
   Using Git and Bitbucket.
   Introduction to Fortran 90
   Makefiles
   Basic Python / perl
   NumPy and Scipy
   Debugging Python
   Compiled vs. interpreted languages
   Computer architecture: CPU, memory access, cache hierachy, pipelining
   Optimizing Fortran
   BLAS and LAPACK routines
   Parallel computing
   OpenMP with Fortran
   MPI with Fortran
   Parallel Python
   Graphics and visualization
   I/O, Binary output
   ODE methods
   Basic concepts of parallelism 
   Parallel matrix computations 
   BLAS and memory hierarchy 
   Finite difference methods
   
