.. _lab1:

===============================
Laboratory Session 1 
===============================

 - **If you get stuck, ask for help from me or David.**
 - Feel free to talk to each other and help each other as needed.
 - Use your imagination to extend the assignments and experiment.


Intro to scripting and :math:`\LaTeX`
-------------------------------------

In this lab we will take a first look at automating tasks using scripting languages. Here we will focus on Perl and how it can be used to compile different pieces of information into a small report. To write the report we will use :math:`\LaTeX`.

 1. Start by adding a directory called ``lab/lab1`` to your repository, this is where you will work on this assignment. This lab work will not be graded but I will check that you did it.

 2. Pull the latest changes from the class repo. If you have not yet cloned it, do so by ``git clone https://your_username@bitbucket.org/appelo/math-471.git``.

 3. Copy the contents of ``math-471/lab/lab1`` into the directory you just created.

 4. Start with opening the file ``lab1.tex`` using, for example, emacs ``>> emacs lab1.tex &``. Inside the file you can see different sections, subsections and some basic commands for equations and figures. To compile the file execute ``>> pdflatex lab1.tex`` in the terminal window (f you are not used to interacting with the computer via a shell you can take a look at the page on :ref:`linux`). After having executed this command you should have generated a file called ``lab1.pdf``, do ``>> ls -ltr`` to check that it is there. Open the pdf file and try compare the output with the ``lab1.tex`` file.

 5. The two obvious pieces of (eh... scientific...) data in this document are the figure and the table and we will get to these in a second. Before discussing how we can generate the table and figure automatically we first turn to a very common task in computing.


Automating a parameter study
++++++++++++++++++++++++++++

One of the most common tasks in computational science is to perform experiments for a range of parameters, say temperaures in a chemical reaction model or number of realizations in a study containing a random variable. Typically this process contains all or some of the steps:

     - Change a data file or a source file.  

     - Compile the program and run it.

     - Post process the output. Perhaps you are computing a length :math:`L` but what you are really interested in reporting is the area :math:`L^2`.    

     - Compile the processed results into tables and/or figures. 
    
If you perform these task by point-and-click this will typically be a painful and slow process. In addition it will be very error prone as it is easy fo forget to do one of the steps, especially if you are interrupted during the process. It will be much more reliable (and faster) if you let the computer do these tasks for you.   
       
We focus on the two first tasks and use the ``lab1.txt`` document as an example of a source code that we would like to change and eventually compile and look at (the pdf version of it). 
       
 6. Open the file ``lab1.tex.Template`` (which should look identical to ``lab1.tex``). Your first task will be to replace the text strings ``AUTHOR1`` and ``AUTHOR2`` with something more appropriate (For example your name). A possible way to do this is by using Perl. The perl script ``lab1.perl`` is set up to replace the first author. Execute ``>> perl lab1.perl`` and take a look at the output on the screen and inside the generated file ``lab1_auto.tex``. Add the replacement of the second author to the perl script. See also the technical topic page about :ref:`PERL`. 

 7. Now you have learnt how to replace a string with another string in a text file, an easy operation but often very useful. Next, inspect the bottom of the per script, you'll see the statement ``system("diff lab1.tex lab1_auto.tex");`` which shows how to execute a system command as if you did it in the shell. Extend the script to also compile the file into a pdf file and, if possible, open it.

 8. Next open the file ``lab1.m`` and change the figure so that the graphics look a bit better. Try to run the matlab script directly from the perl script by the command ``system("nohup matlab -nosplash -nodisplay < lab1.m > output.txt");``   

 9. Open up a matlab window and look at the help page for ``fprintf``. Follow the example with the table of the exponential function and create another table for your :math:`\LaTeX` file. Either copy part of the ''meal'' table and input a modified exp.txt. The ``&`` splits columns and ``\\`` makes a newline in :math:`\LaTeX`, see also meals.tex

 10. If you are done with the assignments make sure you commit and push them to your repository. Feel free to help your friends if you finish early, or start looking at Homework 2.
    
    
